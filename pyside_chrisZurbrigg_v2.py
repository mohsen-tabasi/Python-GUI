from PySide import QtCore, QtGui;
from shiboken import wrapInstance;

import maya.cmds as mc;
import maya.OpenMayaUI as omui;

def getMayaWindow():
    mainWinPtr = omui.MQtUtil.mainWindow();
    return wrapInstance(long(mainWinPtr), QtGui.QWidget);

class primUI(QtGui.QWidget):
    # Singleton pattern
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(primUI, cls).__new__(cls);
        return cls.instance;
    # initializer
    def __init__(self, parent=getMayaWindow()):
        super(primUI, self).__init__(parent);
        self.createUI();
    # create layout
    def createUI(self):
        # main window
        self.setWindowTitle("Make Primitive");
        self.setMinimumSize(100, 100);
        self.setMaximumSize(100, 200);
        self.setWindowFlags(QtCore.Qt.Tool);
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose);
        # main vertical layout
        mainLayout = QtGui.QVBoxLayout();
        mainLayout.setContentsMargins(2,2,2,2);
        mainLayout.setSpacing(2);
        
        # buttons
        self.cubeBtn = QtGui.QPushButton("Cube");
        self.cubeBtn.clicked.connect(primUI.makeCube);
        self.sphereBtn = QtGui.QPushButton("Sphere");
        self.sphereBtn.clicked.connect(primUI.makeSphere);
        self.coneBtn = QtGui.QPushButton("Cone");
        self.coneBtn.clicked.connect(primUI.makeCone);
        self.cylinderBtn = QtGui.QPushButton("Cylinder");
        self.cylinderBtn.clicked.connect(primUI.makeCylinder);
        
        # layouting
        self.setLayout(mainLayout);
        mainLayout.addWidget(self.cubeBtn);
        mainLayout.addWidget(self.sphereBtn);
        mainLayout.addWidget(self.coneBtn);
        mainLayout.addWidget(self.cylinderBtn);
        mainLayout.addStretch();
        
    @staticmethod
    def makeCube():
        mc.polyCube();
        
    @staticmethod
    def makeSphere():
        mc.polySphere();
        
    @staticmethod
    def makeCone():
        mc.polyCone();
        
    @staticmethod
    def makeCylinder():
        mc.polyCylinder();
    

try :
    ui.close();
    ui.deleteLater();
except:
    pass;
ui = primUI()
ui.show();