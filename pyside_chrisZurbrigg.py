from PySide import QtCore, QtGui;
from shiboken import wrapInstance;

import maya.cmds as mc;
import maya.OpenMayaUI as omui;


def mainWindow():
    mainWinPtr = omui.MQtUtil.mainWindow();
    return wrapInstance(long(mainWinPtr), QtGui.QWidget);


class primitiveUi(QtGui.QDialog):
    def __init__(self, parent=mainWindow()):
        super(primitiveUi, self).__init__(parent);
        self.setWindowTitle("Primitive");
        self.setWindowFlags(QtCore.Qt.Tool);
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose);
        self.setFixedSize(100, 100);

        self.createLayout();

    def createLayout(self):
        # main Layout
        mainLayout = QtGui.QVBoxLayout();
        mainLayout.setContentsMargins(2, 2, 2, 2);
        mainLayout.setSpacing(2);
        mainLayout.addStretch();
        # button
        cubeBtn = QtGui.QPushButton("Cube");
        sphereBtn = QtGui.QPushButton("Sphere");
        torusBtn = QtGui.QPushButton("Torus");
        planeBtn = QtGui.QPushButton("Plane");
        #
        mainLayout.addWidget(cubeBtn);
        mainLayout.addWidget(sphereBtn);
        mainLayout.addWidget(torusBtn);
        mainLayout.addWidget(planeBtn);
        self.setLayout(mainLayout);

if __name__ == "__main__":
    try:
        ui.close();
    except:
        pass;

    ui = primitiveUi();
    ui.show();