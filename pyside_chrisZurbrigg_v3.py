from PySide import QtCore, QtGui;
from shiboken import wrapInstance;

import maya.cmds as mc;
import maya.OpenMayaUI as omui;

def getMayaWindow():
    mainWinPtr = omui.MQtUtil.mainWindow();
    return wrapInstance(long(mainWinPtr), QtGui.QWidget);


class testUI(QtGui.QWidget):
    
    # make custom signal
    testSignal = QtCore.Signal();
    
    
    # Singleton pattern
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(testUI, cls).__new__(cls);
        return cls.instance;
        
    # initializer
    def __init__(self, parent=getMayaWindow()):
        super(testUI, self).__init__(parent);
        self.createUI();
        
    # create layout
    def createUI(self):
        
        # main window
        self.setWindowTitle("Test UI");
        self.setWindowFlags(QtCore.Qt.Tool);
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose);
        
        # main vertical layout
        mainLayout = QtGui.QVBoxLayout();
        mainLayout.setContentsMargins(6,6,6,6);
        checkBoxesLayout = QtGui.QHBoxLayout();
        
        # controllers
        self.pushBtn = QtGui.QPushButton("Close");
        self.pushBtn.clicked.connect(self.onBtnPressed);
        self.pushBtn.setToolTip("Exit");
        
        self.checkBoxA = QtGui.QCheckBox("CheckBox A");
        self.checkBoxA.toggled.connect(self.onCheckboxToggled);
        
        self.checkBoxB = QtGui.QCheckBox("CheckBox B");
        self.checkBoxB.toggled.connect(self.onCheckboxToggled);
        
        self.textBox = QtGui.QLineEdit("Line Edit");
        #self.textBox.textChanged.connect(self.onTextChanged);
        self.textBox.editingFinished.connect(self.onTextChanged);
        
        self.list = QtGui.QListWidget();
        self.list.addItems(["item-1", "item-2", "item-3", "item-4", "item-5"]);
        self.list.currentItemChanged.connect(self.onSelectionChanged);
        
        self.list.setCurrentRow(0);
        self.list.setMaximumHeight(100);
        
        # layouting
        self.setLayout(mainLayout);
        mainLayout.addWidget(self.pushBtn);
        mainLayout.addLayout(checkBoxesLayout);
        checkBoxesLayout.addWidget(self.checkBoxA);
        checkBoxesLayout.addWidget(self.checkBoxB);
        mainLayout.addWidget(self.textBox);
        mainLayout.addWidget(self.list);
        mainLayout.addStretch();
        
        # connect testSignal
        self.testSignal.connect(self.testSignalFunc);
        
        # place window in specific point on desktop
        qRect = self.frameGeometry();
        centerPoint = QtGui.QDesktopWidget().availableGeometry().center();
        qRect.moveCenter(centerPoint);
        self.move(qRect.topLeft());
        print(qRect.topLeft(), centerPoint);
    # SLOTS
    def onBtnPressed(self):
        print("Button Pressed");
        # implement test signal
        self.testSignal.emit();
        
        
    def onCheckboxToggled(self):
        sender = self.sender();
        print("Checkbox Toggled By : {}".format(sender.text()));
        
    def onTextChanged(self):
        print("Text Changed");
        
    def onSelectionChanged(self, current, previous):
        print("Selection Changed");
        print("current = {}".format(current.text()));
        print("previous = {}".format(previous.text()));
        
    def testSignalFunc(self):
        print("Test Signal Pressed");
        
        
    # FUNCTIONS
    @staticmethod
    def makeCube():
        mc.polyCube();
        
    @staticmethod
    def makeSphere():
        mc.polySphere();
        
    @staticmethod
    def makeCone():
        mc.polyCone();
        
    @staticmethod
    def makeCylinder():
        mc.polyCylinder();
    

try :
    ui.close();
    ui.deleteLater();
except:
    pass;
ui = testUI()
ui.show();