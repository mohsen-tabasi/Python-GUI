from PySide import QtCore, QtGui;
from shiboken import wrapInstance;

import maya.cmds as mc;
import maya.OpenMayaUI as omui;

def getMayaWindow():
    mainWinPtr = omui.MQtUtil.mainWindow();
    return wrapInstance(long(mainWinPtr), QtGui.QWidget);

class primUI(QtGui.QMainWindow):
    # Singleton pattern
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(primUI, cls).__new__(cls);
        return cls.instance;
    # initializer
    def __init__(self, parent=getMayaWindow()):
        super(primUI, self).__init__(parent);
        self.createUI();
    # create layout
    def createUI(self):
        # main window
        self.setWindowTitle("Calendar Demo");
        self.setWindowFlags(QtCore.Qt.Tool);
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose);
        self.setGeometry(100,100,340,400);
        # calendar
        self.cal = QtGui.QCalendarWidget(self);
        self.cal.setGridVisible(True);
        self.cal.setGeometry(20,20, 300, 300);
        self.date = self.cal.selectedDate();
        self.cal.clicked[QtCore.QDate].connect(self.showDate);
        
        # title
        self.label = QtGui.QLabel(self);
        self.label.setText(self.date.toString());
        self.label.setGeometry(100, 320, 100, 50);
        # layouting
        mainLayout = QtGui.QVBoxLayout();
        self.setLayout(mainLayout);
        mainLayout.addWidget(self.cal);
        #mainLayout.addWidget(self.label);
        
    def showDate(self):
        self.date = self.cal.selectedDate();
        self.label.setText(self.date.toString());

try :
    ui.close();
    ui.deleteLater();
except:
    pass;
ui = primUI()
ui.show();