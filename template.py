from PySide import QtCore, QtGui;
from shiboken import wrapInstance;

import maya.cmds as mc;
import maya.OpenMayaUI as omui;

def getMayaWindow():
    mainWinPtr = omui.MQtUtil.mainWindow();
    return wrapInstance(long(mainWinPtr), QtGui.QWidget);

class primUI(QtGui.QMainWindow):
    # Singleton pattern
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(primUI, cls).__new__(cls);
        return cls.instance;
    # initializer
    def __init__(self, parent=getMayaWindow()):
        super(primUI, self).__init__(parent);
        self.createUI();
    # create layout
    def createUI(self):
        # main window
        self.setWindowTitle("Intro UI");
        self.setWindowFlags(QtCore.Qt.Tool);
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose);

try :
    ui.close();
    ui.deleteLater();
except:
    pass;
ui = primUI()
ui.show();