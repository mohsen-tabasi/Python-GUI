from PySide import QtCore, QtGui;
from shiboken import wrapInstance;

import maya.cmds as mc;
import maya.OpenMayaUI as omui;

def getMayaWindow():
    mainWinPtr = omui.MQtUtil.mainWindow();
    return wrapInstance(long(mainWinPtr), QtGui.QWidget);


class testUI(QtGui.QWidget):
    
    # make custom signal
    testSignal = QtCore.Signal();
    
    
    # Singleton pattern
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(testUI, cls).__new__(cls);
        return cls.instance;
        
    # initializer
    def __init__(self, parent=getMayaWindow()):
        super(testUI, self).__init__(parent);
        self.createUI();
        
    # create layout
    def createUI(self):
        
        # main window
        self.setWindowTitle("Test UI");
        self.setWindowFlags(QtCore.Qt.Tool);
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose);
        
        # main vertical layout
        mainLayout = QtGui.QVBoxLayout();
        mainLayout.setContentsMargins(6,6,6,6);
        checkBoxesLayout = QtGui.QHBoxLayout();
        
        # controllers
        self.pushBtn = QtGui.QPushButton("Close");
        self.pushBtn.clicked.connect(self.onBtnPressed);
        self.pushBtn.setToolTip("Exit");
        self.aboutBtn = QtGui.QPushButton("About");
        self.aboutBtn.clicked.connect(self.onAboutBtnPressed);
        
        # layouting
        self.setLayout(mainLayout);
        mainLayout.addWidget(self.pushBtn);
        mainLayout.addWidget(self.aboutBtn);
        mainLayout.addStretch();
        
    # SLOTS
    # Exit
    def onBtnPressed(self):
        self.msg = QtGui.QMessageBox.question(self, "title", "MSG", QtGui.QMessageBox.Yes, QtGui.QMessageBox.No);
        if (self.msg == QtGui.QMessageBox.Yes): self.close();
    # About
    def onAboutBtnPressed(self):
        self.msg = QtGui.QMessageBox.about(self, "title", "MSG");
        

try :
    ui.close();
    ui.deleteLater();
except:
    pass;
ui = testUI()
ui.show();